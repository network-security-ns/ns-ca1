from socket import *
import threading
import json
import os
import base64
from datetime import datetime, timedelta
import time

MAX_SIZE = 2048

class Server:
    
    def __init__(self):
        self.requestChannel = 9123
        self.loadConfiguration()
        self.startServer()

    def loadConfiguration(self):
        configFile = open("config.json").read()
        configs = json.loads(configFile)
        self.assignConfigs(configs)

    def assignConfigs(self, configs):
        self.clients = {}
        for client in configs['clients']:
            self.clients[client['id']] = [client['port'], client['key']]

    def decodeWithKey(self, key, enc):
        dec = []
        key = str(key)
        enc = base64.urlsafe_b64decode(enc).decode("utf-8")
        for i in range(len(enc)):
            key_c = key[i % len(key)]
            dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
            dec.append(dec_c)
        return "".join(dec)

    def checkDecodedPacketInfo(self, decodedPacket):
        clientT, toBeConnectedClientId, betweenClientKey = decodedPacket.split(",")
        now = datetime.now()
        nowT = str(now.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        nowTimeStamp = datetime.strptime(nowT, "%d-%b-%Y (%H:%M:%S.%f)")
        clientTimeStamp = datetime.strptime(clientT, "%d-%b-%Y (%H:%M:%S.%f)")
        if clientTimeStamp < nowTimeStamp:
            return False, "ERR", "ERR"
        return True, toBeConnectedClientId, betweenClientKey
        
    def encodeWithKey(self, key, clear):
        enc = []
        key = str(key)
        for i in range(len(clear)):
            key_c = key[i % len(key)]
            enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
            enc.append(enc_c)
        return base64.urlsafe_b64encode("".join(enc).encode("utf-8")).decode("utf-8")   
    
    def createEncodedPacketForClient(self, requestingClientId, betweenClientKey, currentClientServerKey):
        now = datetime.now()
        t = now + timedelta(minutes = 1)
        timeStamp = str(t.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        packet = timeStamp + "," + requestingClientId + "," + betweenClientKey
        encodedPacket = self.encodeWithKey(currentClientServerKey, packet)
        return encodedPacket
    
    def handleClient(self, clientRequestSocket):
        packet = clientRequestSocket.recv(MAX_SIZE).decode("utf-8")
        currentClientId, encodedPacket = packet.split(',')

        sock, requestingClientServerKey = self.clients[currentClientId]
        decodedPacket = self.decodeWithKey(requestingClientServerKey, encodedPacket)
        isOk, toBeConnectedClientId, betweenClientKey = self.checkDecodedPacketInfo(decodedPacket)
        if isOk:
            responseChannel, toBeConnectedClientServerKey = self.clients[toBeConnectedClientId]
            self.serverResponseSocket = socket(AF_INET, SOCK_STREAM)
            self.serverResponseSocket.connect(("127.0.0.1", responseChannel))
            encodedPacketForClient = \
                self.createEncodedPacketForClient(currentClientId, betweenClientKey, toBeConnectedClientServerKey)
            self.serverResponseSocket.sendall(encodedPacketForClient.encode("utf-8"))

    def startServer(self):
        self.serverRequestSocket = socket(AF_INET, SOCK_STREAM)
        self.serverRequestSocket.bind(("127.0.0.1", self.requestChannel))
        self.serverRequestSocket.listen(5)
        clientRequestSocket, clientAddress = self.serverRequestSocket.accept()
        self.handleClient(clientRequestSocket)
        clientRequestSocket.close()


s = Server()