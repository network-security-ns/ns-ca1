from socket import *
import sys
import os
from binascii import hexlify
from datetime import datetime, timedelta
import base64
from cryptography.fernet import Fernet


MAX_SIZE = 2048
KEY_SIZE = 256

class Client:

    def __init__(self, serverPort, clientId, clientPort, clientServerKey, isRequesting):
        self.requestChannel = serverPort
        self.clientId = clientId
        self.responseChannel = clientPort
        self.clientServerKey = clientServerKey
        self.isRequesting = isRequesting
        self.startClient()

    def generateKey(self):
        key = Fernet.generate_key().decode()    
        return key    

    def decodeWithKey(self, key, enc):
        dec = []
        key = str(key)
        enc = base64.urlsafe_b64decode(enc).decode("utf-8")
        for i in range(len(enc)):
            key_c = key[i % len(key)]
            dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
            dec.append(dec_c)
        return "".join(dec)

    def checkDecodedPacketInfo(self, decodedPacket):
        serverT, requestingClientId, betweenClientKey = decodedPacket.split(",")
        now = datetime.now()
        nowT = str(now.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        serverTimeStamp = datetime.strptime(serverT, "%d-%b-%Y (%H:%M:%S.%f)")
        nowTimeStamp = datetime.strptime(nowT, "%d-%b-%Y (%H:%M:%S.%f)")
        if serverTimeStamp < nowTimeStamp:
            return False, "ERR", "ERR"
        return True, requestingClientId, betweenClientKey

    def encodeWithKey(self, key, clear):
        enc = []
        key = str(key)
        for i in range(len(clear)):
            key_c = key[i % len(key)]
            enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
            enc.append(enc_c)
        return base64.urlsafe_b64encode("".join(enc).encode("utf-8")).decode("utf-8")   
    
    def createEncodedPacketForServer(self, toBeConnectedClientId):
        now = datetime.now()
        t = now + timedelta(minutes = 1)
        timeStamp = str(t.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        betweenClientKey = str(self.generateKey())
        print(str(betweenClientKey))
        packet = timeStamp + "," + toBeConnectedClientId + "," + betweenClientKey
        encodedPacket = self.encodeWithKey(self.clientServerKey, packet)
        return encodedPacket

    def startClient(self):
        self.clientResponseSocket = socket(AF_INET, SOCK_STREAM)
        self.clientResponseSocket.bind(("127.0.0.1", self.responseChannel))
        self.clientResponseSocket.listen(5)

        if (self.isRequesting == "r"):
            self.clientRequestSocket = socket(AF_INET, SOCK_STREAM)
            self.clientRequestSocket.connect(("127.0.0.1", self.requestChannel))
            command = input()
            commandTokens = command.split()
            packet = self.clientId + "," + self.createEncodedPacketForServer(commandTokens[1])
            self.clientRequestSocket.sendall(packet.encode("utf-8"))
        
        else:
            serverResponseSocket, serverResponseAddress = self.clientResponseSocket.accept()

            encodedPacket = serverResponseSocket.recv(MAX_SIZE).decode("utf-8")
            decodedPacket = self.decodeWithKey(self.clientServerKey, encodedPacket)
            isOk, requestingClientId, betweenClientKey = self.checkDecodedPacketInfo(decodedPacket)
            if isOk:
                print(str(betweenClientKey))
            serverResponseSocket.close()


serverPort = int(sys.argv[1])
clientId = sys.argv[2]
clientPort = int(sys.argv[3])
clientServerKey = int(sys.argv[4])
isRequesting = sys.argv[5]
newClient = Client(serverPort, clientId, clientPort, clientServerKey, isRequesting)