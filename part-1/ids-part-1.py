import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import os
import sys


blackListPath = sys.argv[1]
pcapPath = sys.argv[2]

if not os.path.exists(blackListPath):
    exit("The path to blacklist is not correct!")
elif os.path.isdir(blackListPath):
    blackListPath += "/blacklist.txt"
if not os.path.exists(pcapPath):
    exit("The path to pcaps is not correct!")

blackListFile = open(blackListPath)
blacks = blackListFile.read().splitlines()
pcapFiles = os.listdir(pcapPath)
blackPcs = []

for file in pcapFiles:
    filePath = pcapPath + "/" + file
    dirty = False
    packets = PcapReader(filePath)
    for packet in packets:
        if packet.haslayer(IP):
            packetSrc = packet[IP].src
            packetDst = packet[IP].dst
            if packetDst in blacks:
                dirty = True
        if packet.haslayer(DNSQR):
            dnsAnsCnt = packet.ancount
            if dnsAnsCnt > 0:
                packetDomain = (packet[DNSQR].qname).decode("utf-8")
                if packetDomain[-1] == '.':
                    packetDomain = packetDomain[:-1]
                for i in range(dnsAnsCnt):
                    if packet.an[i].type == 1:
                        domainIp = packet.an[i].rdata
                        blacks.append(domainIp)
    if dirty == True:
        fileData = file[:-5]
        pcName, pcIp = fileData.split('-')
        blackPcs.append(pcName + ',' + pcIp)

if len(blackPcs) > 0:
    for pc in blackPcs:
        print(pc)

# if len(blackPcs) > 0:
#     for p in range(len(blackPcs) - 1):
#         print(blackPcs[p])
#     print(blackPcs[-1], end='')
    