import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import os
import sys


pcapPath = sys.argv[1]

if not os.path.exists(pcapPath):
    exit("The path to pcap is not correct!")

packets = PcapReader(pcapPath)
answers = []

for packet in packets:
    if packet.haslayer(IP) and packet.haslayer(TCP):
        packetHex = bytes_hex(packet).decode("utf-8")
        if "48545450" in packetHex:
            if "474554" in packetHex or "504f5354" in packetHex or \
              "505554" in packetHex or "44454c455445" in packetHex or \
              "48454144" in packetHex or "434f4e4e454354" in packetHex or \
              "4f5054494f4e53" in packetHex or "5452414345" in packetHex or \
              "5041544348" in packetHex:
                packetDstIp = packet[IP].dst
                packetDstPort = packet[TCP].dport
                ans = str(packetDstIp) + "," + str(packetDstPort)
                if ans not in answers:
                    answers.append(ans)

if len(answers) > 0:
    for a in answers:
        print(a)

# if len(answers) > 0:
#     for a in range(len(answers) - 1):
#         print(answers[a])
#     print(answers[-1], end='')
