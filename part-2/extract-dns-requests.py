import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import os
import sys


pcapPath = sys.argv[1]

if not os.path.exists(pcapPath):
    exit("The path to pcap is not correct!")

packets = PcapReader(pcapPath)
answers = []

for packet in packets:
    if packet.haslayer(UDP):
        srcPort = packet[UDP].sport
        dstPort = packet[UDP].dport
        if srcPort == 53:
            domainName = (packet[DNSQR].qname).decode("utf-8")
            if domainName[-1] == '.':
                    domainName = domainName[:-1]
            dnsAnsCnt = packet.ancount
            if dnsAnsCnt > 0:
                for i in range(dnsAnsCnt):
                    if packet.an[i].type == 1:
                        domainIp = packet.an[i].rdata
                        ans = domainName + "," + domainIp
                        if ans not in answers:
                            answers.append(ans)

if len(answers) > 0:
    for a in answers:
        print(a)

# if len(answers) > 0:
#     for a in range(len(answers) - 1):
#         print(answers[a])
#     print(answers[-1], end='')
